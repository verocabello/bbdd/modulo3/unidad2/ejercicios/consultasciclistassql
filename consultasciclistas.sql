﻿/**
  CONSULTAS DE SELECCION 1
  
  */

  USE ciclistas;

-- SELECCION 1

SELECT DISTINCT edad FROM ciclista;

SELECT count(DISTINCT edad) FROM ciclista WHERE nomequipo='Artiach' OR nomequipo='Amore Vita';

SELECT dorsal FROM ciclista WHERE edad<25 OR edad>30;

SELECT * FROM ciclista WHERE edad BETWEEN 28 AND 32 AND nomequipo='Banesto';

SELECT nombre FROM ciclista WHERE LENGTH(nombre)>8;

SELECT nombre,dorsal,UPPER(nombre) FROM ciclista;

SELECT DISTINCT dorsal FROM lleva WHERE código='MGE';

SELECT nompuerto FROM puerto WHERE altura>1500;

SELECT DISTINCT dorsal FROM puerto WHERE altura BETWEEN 1800 AND 3000 OR pendiente>8;

SELECT DISTINCT dorsal FROM puerto WHERE altura BETWEEN 1800 AND 3000 AND pendiente>8;


-- SELECCION 2
-- 1
SELECT COUNT(*) FROM ciclista;
-- 2
SELECT COUNT(*) FROM ciclista WHERE nomequipo='Banesto';
-- 3
SELECT AVG(edad) FROM ciclista;
-- 4
SELECT AVG(edad) FROM ciclista WHERE nomequipo='Banesto';
-- 5
SELECT nomequipo,AVG(edad) FROM ciclista GROUP BY nomequipo;
-- 6
SELECT nomequipo,COUNT(*) FROM ciclista GROUP BY nomequipo;
-- 7
SELECT COUNT(*) FROM puerto;
-- 8
SELECT COUNT(*) FROM puerto WHERE altura>1500;
-- 9
SELECT nomequipo FROM ciclista GROUP BY nomequipo HAVING COUNT(*)>4;
-- 10
SELECT nomequipo FROM ciclista WHERE edad BETWEEN 28 AND 32 GROUP BY nomequipo HAVING COUNT(*)>4;
-- 11
SELECT dorsal,COUNT(*) FROM etapa GROUP BY dorsal;
-- 12
SELECT dorsal FROM etapa GROUP BY dorsal HAVING COUNT(*)>1;



-- SELECCION 3
-- 3
SELECT dorsal FROM ciclista WHERE nomequipo='Banesto' AND edad BETWEEN 25 AND 32;
-- 4
SELECT dorsal FROM ciclista WHERE nomequipo='Banesto' OR edad BETWEEN 25 AND 32;
-- 8
SELECT nompuerto FROM puerto WHERE altura BETWEEN 1000 AND 2000 OR altura>2400;
-- 12
SELECT COUNT(DISTINCT dorsal) FROM puerto;
-- 14
SELECT AVG(altura) FROM puerto;
-- 18
SELECT dorsal,código,COUNT(*) FROM lleva GROUP BY dorsal,código;



-- SELECCION 4
-- 1
SELECT DISTINCT nombre,edad FROM etapa JOIN ciclista ON etapa.dorsal=ciclista.dorsal;

SELECT DISTINCT nombre,edad FROM etapa JOIN ciclista USING(dorsal);
-- 2
SELECT DISTINCT nombre,edad FROM puerto JOIN ciclista USING(dorsal);
-- 3
SELECT DISTINCT nombre,edad FROM ciclista JOIN etapa USING(dorsal) JOIN puerto USING(dorsal);
-- 4
SELECT DISTINCT equipo.director FROM etapa JOIN ciclista USING(dorsal) JOIN equipo USING(nomequipo);
-- 5
SELECT DISTINCT dorsal,nombre FROM ciclista JOIN lleva USING(dorsal);
-- 6
SELECT DISTINCT dorsal,nombre FROM ciclista JOIN lleva USING(dorsal) JOIN maillot USING(código) WHERE color='amarillo';
-- 7
SELECT DISTINCT dorsal FROM lleva JOIN etapa USING(dorsal);
-- 8
SELECT DISTINCT numetapa FROM puerto;
-- 9
SELECT DISTINCT kms,etapa.numetapa FROM etapa JOIN ciclista USING(dorsal) JOIN puerto USING(numetapa) WHERE ciclista.nomequipo='Banesto';
-- 10
SELECT DISTINCT ciclista.dorsal,ciclista.nombre FROM etapa JOIN puerto USING(numetapa) JOIN ciclista ON ciclista.dorsal=etapa.dorsal;
-- 11
SELECT nompuerto FROM puerto JOIN ciclista USING(dorsal) WHERE nomequipo='Banesto';
-- 12
SELECT DISTINCT numetapa FROM puerto JOIN ciclista USING(dorsal) JOIN etapa USING(numetapa) WHERE nomequipo='Banesto' AND kms>200;


-- SELECCION 5
-- 1
SELECT DISTINCT nombre,edad FROM ciclista LEFT JOIN etapa ON ciclista.dorsal=etapa.dorsal WHERE etapa.dorsal IS NULL;
-- 2
SELECT DISTINCT nombre,edad FROM ciclista LEFT JOIN puerto ON ciclista.dorsal=puerto.dorsal WHERE puerto.dorsal IS NULL;
-- 3
SELECT DISTINCT director FROM equipo JOIN ciclista USING(nomequipo) LEFT JOIN etapa ON ciclista.dorsal=etapa.dorsal WHERE etapa.dorsal IS NULL;
-- 4
SELECT DISTINCT ciclista.dorsal,ciclista.nombre FROM ciclista LEFT JOIN lleva ON ciclista.dorsal=lleva.dorsal WHERE lleva.dorsal IS NULL;
-- 5
c1
SELECT DISTINCT dorsal FROM lleva JOIN maillot USING(código) WHERE color='amarillo';

SELECT ciclista.dorsal,ciclista.nombre FROM ciclista LEFT JOIN (
  SELECT DISTINCT dorsal FROM lleva JOIN maillot USING(código) WHERE color='amarillo') c1
  ON ciclista.dorsal=c1.dorsal 
    WHERE c1.dorsal IS NULL;
-- 6
SELECT DISTINCT etapa.numetapa FROM etapa LEFT JOIN puerto ON etapa.numetapa=puerto.numetapa WHERE puerto.numetapa IS NULL;
-- 7
SELECT AVG(etapa.kms) FROM etapa LEFT JOIN puerto ON etapa.numetapa=puerto.numetapa WHERE puerto.numetapa IS NULL;
-- 8
SELECT COUNT(ciclista.dorsal) FROM ciclista LEFT JOIN etapa ON ciclista.dorsal=etapa.dorsal WHERE etapa.dorsal IS NULL;
-- 9
SELECT DISTINCT ciclista.dorsal FROM ciclista JOIN etapa USING(dorsal) LEFT JOIN puerto ON etapa.numetapa=puerto.numetapa WHERE puerto.numetapa IS NULL;
-- 10
SELECT DISTINCT ciclista.dorsal FROM ciclista JOIN etapa USING(dorsal) LEFT JOIN puerto ON etapa.numetapa=puerto.numetapa WHERE puerto.numetapa IS NULL;


